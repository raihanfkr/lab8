from django.apps import AppConfig


class Lab8AppConfig(AppConfig):
    name = 'lab8app'
