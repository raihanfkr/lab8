function bookSearch(){
    var search = document.getElementById('search').value
    document.getElementById('results').innerHTML = ""
    console.log(search)

    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=" + search,
        dataType: "json",

        success: function(data){
            $('tbody').html('')
            var stringHTML = '<tr>';

            for (i = 0; i < data.items.length; i++){
            //     results.innerHTML += "<h2 class='align-middle text-center'>" + data.items[i].volumeInfo.title + "</h2>"
            //     results.innerHTML += "<h3 class='align-middle'>" + data.items[i].volumeInfo.authors + "</h3>"
            //     results.innerHTML += "<image src= "+ data.items[i].volumeInfo.imageLinks.thumbnail + ">"  
            // }

            stringHTML += "<tr> <th scope='row' class='align-middle text-center'>" + (i + 1) + "</th>" +
                        "<td><image src='" + data.items[i].volumeInfo.imageLinks.thumbnail + "'></image>" + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.title + "</td>" +
                        "<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>"
            }
            $('tbody').append(stringHTML);
        },

        error: function(error) {
            alert("Books not found");
        },

        type: 'GET'
    })
}

document.getElementById("button").addEventListener('click', bookSearch, false)